/*task1 */
SELECT p.prod_category, SUM(s.amount_sold) AS total_costs
FROM sh.sales s
JOIN sh.products p ON s.prod_id = p.prod_id
WHERE s.time_id  BETWEEN '1998-01-01' AND '1998-01-12'
GROUP BY p.prod_category;

/*task 2*/
SELECT
    c.country_region AS region,
    AVG(s.quantity_sold) AS avg_sales_quantity
FROM
    sales s
JOIN
    customers cu ON s.cust_id = cu.cust_id
JOIN
    countries c ON cu.country_id = c.country_id
WHERE
    s.prod_id = '17'
GROUP BY
    c.country_region;

   
   /*task 3*/
   
   SELECT
    c.cust_id,
    c.cust_first_name,
    c.cust_last_name,
    SUM(s.amount_sold) AS total_sales_amount
FROM
    customers c
JOIN
    sales s ON c.cust_id = s.cust_id
GROUP BY
    c.cust_id, c.cust_first_name, c.cust_last_name
ORDER BY
    total_sales_amount DESC
LIMIT 5;



